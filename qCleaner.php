<?php

require "wp-config.php";
define('DB_HOSTING', 'mysql:host='.DB_HOST.';dbname='.DB_NAME);
define('POST_TABLE', 'wp_posts');
define('PRESERVE_LANGUAGE', 'en');
define('charset', 'UTF-8');
define('START_MARKER', '<!--:' . PRESERVE_LANGUAGE . '-->');
define('END_MARKER', '<!--:-->');
mb_internal_encoding(charset);
echo 'connecting to DB...'."<br />";
if(! extension_loaded('pdo') )
	die('PDO extension is required :(');
try {
	$dbh = new PDO(DB_HOSTING, DB_USER, DB_PASSWORD);
}
catch(PDOException $e) {
	die('error connecting to DB. sorry :(');
}

echo 'fetching posts...';
try {
	$rows = $dbh->query('SELECT ID, post_title, post_content FROM ' . POST_TABLE);
}
catch(PDOException $e) {
	echo $e->getMessage();
}
$tobecleansed = array();
foreach($rows->fetchAll(PDO::FETCH_NUM) as $row) {
	list($id, $title, $content) = $row;
	if(  preg_match(START_MARKER, $title) )
		$tobecleansed[$id]['post_title'] = preg_replace("/((?s).*)(".START_MARKER.")(.*)(".END_MARKER.")/", "$3", $title);

	if( preg_match(START_MARKER, $content))
		$tobecleansed[$id]['post_content'] = preg_replace("/((?s).*)(".START_MARKER.")((?s).*)(".END_MARKER.")/", "$3", $content);


}
echo 'cleaning posts...'."<br />";
foreach($tobecleansed as $id=>$post) {
	$update = array();
	$vals = array();
	foreach($post as $property=>$value) {
		$update[] = "$property = :$property";
		$vals[":$property"] = $value;
	}

	$update = implode(', ', $update);
	$vals[':id'] = $id;

	try {
		$stmt = $dbh->prepare("UPDATE " . POST_TABLE . " SET $update WHERE ID = :id");
		$stmt->execute($vals);
		print_r($stmt->errorInfo());
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
try {
	$rows = $dbh->query('SELECT ID, post_title, post_content FROM ' . POST_TABLE);
}
catch(PDOException $e) {
	echo $e->getMessage();
}
$tobecleansed = array();
foreach($rows->fetchAll(PDO::FETCH_NUM) as $row) {
	list($id, $title, $content) = $row;
	if (preg_match('[:' . PRESERVE_LANGUAGE . ']',	$title)) {
		
		
		$tobecleansed[$id]['post_title'] = preg_replace("/((?s).*)(\[" . PRESERVE_LANGUAGE . "\])((?s).*?)(\[:..|:\])/", "$3", $title);

		}

	if (preg_match('[:' . PRESERVE_LANGUAGE . ']', $content)) {
		$tobecleansed[$id]['post_content'] = preg_replace("/((?s).*)(\[" . PRESERVE_LANGUAGE . "\])((?s).*?)(\[:..|:\])/", "$3", $content);

		}
}
foreach($tobecleansed as $id=>$post) {
	$update = array();
	$vals = array();
	foreach($post as $property=>$value) {
		$update[] = "$property = :$property";
		$vals[":$property"] = $value;
	}

	$update = implode(', ', $update);
	$vals[':id'] = $id;

	try {
		$stmt = $dbh->prepare("UPDATE " . POST_TABLE . " SET $update WHERE ID = :id");
		$stmt->execute($vals);
		print_r($stmt->errorInfo());
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
echo 'great success!<br /> Fixed ' . count($tobecleansed) . ' posts';

?>